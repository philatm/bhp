#!/usr/bin/python3
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import time
import sys

PI = 3.1415926
GRAVITY = 9.80655 	#
BASE_DENS = 1000	#Water density; kg/m3
WELL_VOL = 41.9		#Whole well; volume; m3
HEIGHT = 3098		#Clean height; m
LENGTH = 3790		#  ; m
D = 118.6			# Tube diametr; mm
SG = 2.59			# specific gravity; kg/L

#calculated from constants
AREA = 0.25 * PI * (D/1000) ** 2
V_H = HEIGHT * AREA 

#indexes of 
T_IN = 0
PRESS_IN = 1
CONC_IN = 10
SRATE_IN = 6

dt = 1 #every second

def density(conc):
	v = conc/SG
	res = (BASE_DENS+conc)/(BASE_DENS+v)*1000
	return res

def concentration(density):
	#density in kg / m3
	d = density / 1000
	res = BASE_DENS * (d - 1) / (1 - d / SG)
	return res
#tube actually goes from end to start
def put(tube, pdens, pv):
	if pv >= WELL_VOL:
		tube = [[pdens, WELL_VOL]]
		return
	if pv <= tube[0][1]:
		tube[0][1] -= pv
		tube.append([pdens, pv])
		return

	gv = tube[0][1]
	num = 0
	while gv < pv:
		num += 1
		gv += tube[num][1]
	for _ in range(num):
		tube.pop(0)
	tube[0][1] = gv - pv
	tube.append([pdens, pv])

def friction_press(tube, rate):
	#corection factor to concentration. took from excel "Calculations RT"
	def conc_factor(conc):
		return 1 + conc / 1000
	def rate_factor(rate):
		# factor = a*Q^k
		a = 0.015
		k = 0.4
		return a * rate ** k
	res = 0
	for vol in tube:
		dv = vol[1]
		conc = concentration(vol[0])
		res += conc_factor(conc) * dv
	res *= rate_factor(rate)
	res /= AREA
	#res /= 1000
	return res

def hydro_press(tube):
	tmp_v = 0
	cur_in = -1
	dh = 0
	res_p = 0
	while tmp_v < V_H:
		#print("New line - {0}".format(len(tube)), end="\n")
		dv = tube[cur_in][1]
		dens = tube[cur_in][0]
		tmp_v += dv
		dp = dens * GRAVITY * dv #will divide by area in the end
		res_p += dp
		cur_in -= 1
	#substract surplus
	res_p -= dens * GRAVITY * (tmp_v - V_H)
	res_p = res_p / AREA
	res_p = res_p / 10 ** 5 #convert to bars
	return res_p

def plot_bhp(filename):
	plt.style.use("ggplot")
	fig, (ax1, ax3) = plt.subplots(2,1,figsize=(16,8),sharex=True,dpi=80)
	#fig.show()

	#interactive mode on/off
	plt.ioff()

	time_arr = []
	press_arr = []
	conc_arr = []
	srate_arr = []
	h_press_arr = []
	f_press_arr = []
	bhp_press_arr = []
	cnt = 0
	well_tube = [[BASE_DENS, WELL_VOL]]

	try:
		with open(filename) as f:
			for _ in range(2):
				next(f)
			for line in f:
				#line = f.readline()
				if True: # cnt % 20 == 0:
					cur_line = line.split('\t')
					t, press1, conc, srate = [float(x) for x in [cur_line[T_IN], cur_line[PRESS_IN], cur_line[CONC_IN], cur_line[SRATE_IN]]]

					cur_vol = srate * dt / 60 #Rate given per minute need per second
					put(well_tube, density(conc), cur_vol)

					h_press = hydro_press(well_tube)
					f_press = friction_press(well_tube, srate)
					bhp_press = press1 + h_press - f_press


					time_arr.append(t)
					press_arr.append(press1)
					conc_arr.append(conc)
					srate_arr.append(srate)
					h_press_arr.append(h_press)
					f_press_arr.append(f_press)
					bhp_press_arr.append(bhp_press)
	except OSError as err:
		print("there is no such file")
		input("Press enter to exit...")
		sys.exit(1)



	ax1.plot(time_arr, press_arr, color='b', label='Pressure')
	ax1.plot(time_arr, conc_arr, color='r', label='Concentration')
	ax1.plot(time_arr, h_press_arr, color='y', label="Hydrostatic pressure")
	ax3.plot(time_arr, f_press_arr, color='tab:orange', label="Friction pressure")
	ax1.plot(time_arr, bhp_press_arr, color="tab:cyan", label="Bottom Hole pressure")
	ax2 = ax1.twinx()
	ax2.plot(time_arr, srate_arr, color='g', label='Slurry Rate')

	ax2.set_title(filename)
	ax1.legend(loc='upper left')
	ax2.legend(loc='upper right')
	ax3.legend(loc='upper left')
	ax1.set_xlim(time_arr[0]-10, time_arr[-1]+10)
	#show the graph
	plt.show()
	return

def main():
	filename = input("Enter the filename to proceed and press Enter: ")
	plot_bhp(filename)

if __name__ == "__main__":
	main()

'''
def test():
	eps = 0.0001
	if (hydro_press([[BASE_DENS, WELL_VOL]]) - 303.8069) >= eps:
		print("Warnihg!!!!")
	if  (hydro_press([[1000, V_H/2], [1200, V_H/2]]) - 334.1876) >= eps:
		print("warning!!!!!")
test()
'''